provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppet-slave-vishali" {
  ami             = "ami-07d9b9ddc6cd8dd30"
  instance_type   = "t2.micro"
  key_name        = "terraform"
  security_groups = ["assesment_4"] # Specify your security group here

  tags = {
    Name = "puppetagent-vishali"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("terraform.pem")
    host        = self.public_ip
    timeout     = "20m" # Adjust timeout as per your requirement
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.8.152 puppet\" >> /etc/hosts'",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
      "sudo systemctl status puppet",
    ]
  }
}

output "public_ip" {
  value = aws_instance.puppet-slave.public_ip
}
